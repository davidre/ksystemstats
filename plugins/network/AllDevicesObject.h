/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include <systemstats/SensorObject.h>

class NetworkDevice;

namespace KSysGuard
{
    class AggregateSensor;
}

/**
 * This object aggregates the network usage of all devices.
 */
class AllDevicesObject : public KSysGuard::SensorObject
{
    Q_OBJECT

public:
    AllDevicesObject(KSysGuard::SensorContainer* parent);

private:
    KSysGuard::AggregateSensor *m_downloadSensor = nullptr;
    KSysGuard::AggregateSensor *m_uploadSensor = nullptr;
    KSysGuard::AggregateSensor *m_totalDownloadSensor = nullptr;
    KSysGuard::AggregateSensor *m_totalUploadSensor = nullptr;
};
